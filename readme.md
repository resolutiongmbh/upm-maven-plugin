#The UPM Maven Plugin

This Maven-plugin allows installing an Atlassian-plugin into the host-application during the build-process.

### Quick Start
Add Resolution's public Maven-repository to your App's POM:

    <pluginRepositories>
    ...
        <pluginRepository>
            <id>resolution.public.release</id>
            <url>http://public.maven.resolution.de/release</url>
        </pluginRepository>
        <pluginRepository>
            <id>resolution.public.snapshots</id>
            <url>http://public.maven.resolution.de/snapshot</url>
        </pluginRepository>
    </pluginRepositories>
    
Add the upm-maven-plugin to the plugins-section of your POM:    
    
    <plugins>
        ...
        <plugin>
            <groupId>de.resolution</groupId>
            <artifactId>upm-maven-plugin</artifactId>
            <version>1.4.0</version>
        </plugin>
    </plugins>
 
   
Add some parameters about your instance to the properties-section of your POM:

    <properties>
        ...
        <upm.host>https://jira.example.com</upm.host>
        <upm.user>admin</upm.user>
        <upm.password>adminPasswordGoesHere</upm.password>
    </properties>

You can now build and upload your App with `atlas-mvn package upm:reinstall


## Parameters 
Parameters can be specified or overridden on the command-line using e.g. `-Dupm.host=https://your.host.example.com`
If a parameter is not specified, the Environment variable's value is used.

|Name        |Description                               |Default value                 |Environment variable       |
|------------|------------------------------------------|------------------------------|---------------------------|
|upm.host                |Application base url          |                              |UPM_HOST                   |
|upm.user                |Admin-user on the application |                              |UPM_USER                   |
|upm.password            |Password for that user        |                              |UPM_PASSWORD               |
|upm.skip                |Skip the execution            | false                        |                           |
|upm.skip.removeBefore   |Skip removing the plugins in removeBefore before installing| | |
|atlassian.plugin.license|Plugin license                |                              |UPM_LICENSE                |


## Goals
|Name          |Description                         |
|--------------|------------------------------------|
|install       |Install the app                     |
|reinstall     |Remove the app and install it again |
|remove        |Remove the app                      |
|licenseInstall|Install a license for the app       |
|licenseRemove |Remove a licence for the app        |


## Install the app during build
The reinstall-goal can be bound to the pre-integration-test-phase. This allows to automatically install the app 
before starting integration tests. To enable this, add an `execution`-block to the plugin-configuration:

    <plugin>
        <groupId>de.resolution</groupId>
        <artifactId>upm-maven-plugin</artifactId>
        <version>1.4.0</version>
        <executions>
            <execution>
                <id>reinstall_it</id>
                <goals>
                    <goal>reinstall</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    
## Remove other apps before install
You can specify a list of plugin-keys to be removed before installation.

    <plugin>
        <groupId>de.resolution</groupId>
        <artifactId>upm-maven-plugin</artifactId>
        <version>1.3.1</version>
            <configuration>
                <removeBefore>
                    <param>com.example.plugin1</param>
                    <param>com.example.plugin2</param>     
                </removeBefore>    
        </configuration>
    </plugin>
This can be skipped using -Dupm.skip.removeBefore on the command line