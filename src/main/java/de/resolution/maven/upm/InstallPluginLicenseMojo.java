package de.resolution.maven.upm;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="licenseInstall", requiresProject = false)
public class InstallPluginLicenseMojo extends AbstractUpmMojo {

	public void doExecute() throws MojoExecutionException {
		super.installLicense();
	}
}
