package de.resolution.maven.upm;

import de.resolution.upmclient.UpmRestClient;
import de.resolution.upmclient.UpmRestClientException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUpmMojo extends AbstractMojo {

    private static final Logger logger = LoggerFactory.getLogger(AbstractUpmMojo.class);

    @Parameter(defaultValue = "${atlassian.plugin.key}", property = "atlassian.plugin.key")
    private String pluginKey;

    @Parameter(defaultValue = "${env.UPM_LICENSE}", property = "atlassian.plugin.license")
    private String pluginLicenseString;

    @Parameter(property = "upm.file")
    private File file;

    @Parameter(property = "upm.coordinate")
    private String pluginMavenCoordinate;

    /**
     * Plugin keys to remove before installing the plugin itself
     */
    @Parameter(property = "upm.removeBefore")
    private List<String> removeBefore = new ArrayList<>();

    /**
     * Plugins to install before installing this artifact. Either as maven-coordinates group:artifact:version
     * or as a file path
     */
    @Parameter(property = "upm.installBefore")
    private List<String> installBefore = new ArrayList<>();

    /**
     * Plugins to install after this artifact. Either as maven-coordinates group:artifact:version
     * or as a file path
     */
    @Parameter(property = "upm.installAfter")
    private List<String> installAfter = new ArrayList<>();

    /**
     * Plugin keys to remove also when uninstalling or reinstalling this artifact
     */
    @Parameter(property = "upm.removeAlso")
    private String removeAlso;

    @Parameter(defaultValue = "${env.UPM_HOST}", property = "upm.host")
    private String hosturl;

    @Parameter(required = true, defaultValue = "${env.UPM_USER}", property = "upm.user")
    private String userid;

    @Parameter(required = true, defaultValue = "${env.UPM_PASSWORD}", property = "upm.password")
    private String password;

    @Parameter(defaultValue = "${project.build.finalName}")
    private String finalName;

    @Parameter(defaultValue = "${project.packaging}")
    private String packaging;

    @Parameter(defaultValue = "${project.build.directory}")
    private String target;

    @Parameter(defaultValue = "false", property = "upm.skip")
    private boolean skip;

    @Parameter(defaultValue = "false", property = "upm.skip.removeBefore")
    private boolean skipRemoveBefore;

    @SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal"}) // needed for parameter-injection
    @Parameter(defaultValue = "30", property = "upm.timeout")
    private long timeout = 30;

    /**
     * Install <finalName>.jar, even if an obr is present
     */
    @Parameter(required = true, defaultValue = "false", property = "upm.jar")
    private boolean installJar;

    /**
     * The current repository/network configuration of Maven.
     */
    @Parameter(defaultValue = "${repositorySystemSession}")
    private RepositorySystemSession repoSession;

    /**
     * The project's remote repositories to use for the resolution of plugins and their dependencies.
     */
    @Parameter(defaultValue = "${project.remotePluginRepositories}")
    private List<RemoteRepository> remoteRepos;

    /**
     * The entry point to Aether, i.e. the component doing all the work.
     */
    @Component
    private RepositorySystem repoSystem;

    private UpmRestClient upmRestClient;

    /**
     * Initializes the values. Called within execute() but exposed as separate method to be called in tests
     */
    public void initialize() {




        upmRestClient = new UpmRestClient(hosturl, userid, password);


    }

    /**
     * Checks the parameters and calls doExecute()
     *
     * @throws MojoExecutionException if something goes wrong
     */
    public void execute() throws MojoExecutionException {

        if (file == null && pluginMavenCoordinate == null && !packaging.equals("atlassian-plugin")) {
            logger.info("Packaging is not atlassian-plugin, done here");
            return;
        }

        if (skip) {
            logger.info("skip is active, done here");
            return;
        }

        if (hosturl == null) {
            logger.info("No hosturl set, done here");
            return;
        }

        if (file == null && pluginMavenCoordinate == null && pluginKey == null) {
            throw new MojoExecutionException("pluginKey is not set");
        }

        initialize();
        doExecute();
    }

    /**
     * Implementations for the different actions should call the appropriate methods
     *
     * @throws MojoExecutionException if something goes wrong
     */
    public abstract void doExecute() throws MojoExecutionException;


    public void removeLicense() throws MojoExecutionException {
        try {
            upmRestClient.removeLicense(pluginKey);
        } catch (UpmRestClientException e) {
            throw new MojoExecutionException(e.getMessage(),e);
        }
    }

    public void installLicense() throws MojoExecutionException {

        if (pluginLicenseString == null || pluginLicenseString.trim().isEmpty()) {
            throw new MojoExecutionException("No license string provided");
        }

        try {
            upmRestClient.installLicense(pluginKey, pluginLicenseString);
        } catch (UpmRestClientException e) {
            throw new MojoExecutionException(e.getMessage(),e);
        }
    }

    /**
     * Called by doExecute in RemovePluginMojo
     */
    void deletePlugin() throws MojoExecutionException {
        logger.info("Removing plugin {}", this.pluginKey);
        try {
            if (removeAlso != null) {
                upmRestClient.removePlugin(removeAlso);
            }
            upmRestClient.removePlugin(pluginKey);
        } catch (UpmRestClientException.NotFoundException e) {
            logger.info("Plugin {} was not found, so there's nothing to remove.", this.pluginKey);
        }
        catch (UpmRestClientException e) {
            throw new MojoExecutionException(e.getMessage(),e);
        }
    }


    /**
     * Reinstalls the plugin, called by doExecute in the ReinstallPluginMojo
     *
     * @throws MojoExecutionException if somethings goes wrong
     */
    void reinstallPlugin() throws MojoExecutionException {
        deletePlugin();
        installPlugin();
    }

    /**
     * Installs the plugin, called by doExecute InstallPluginMojo
     *
     * @throws MojoExecutionException if anything fails
     */
    void installPlugin() throws MojoExecutionException {

        File pluginFile = this.file;

        if(pluginFile == null && pluginMavenCoordinate != null) {
            pluginFile = getPluginFile(pluginMavenCoordinate);
        }

        if (pluginFile == null) {

            File obrFile = new File(target, finalName + ".obr");
            File jarFile = new File(target, finalName + ".jar");

            if (installJar) {
                if (jarFile.canRead()) {
                    pluginFile = jarFile;
                } else {
                    throw new MojoExecutionException(jarFile.getName() + " cannot be read.");
                }
            } else if (obrFile.canRead()) {
                pluginFile = obrFile;
            } else if (jarFile.canRead()) {
                pluginFile = jarFile;
            } else {
                throw new MojoExecutionException("Neither " + obrFile.getName() + " nor " + jarFile.getName() + " can be read.");
            }
        }

        if (!pluginFile.canRead()) {
            throw new MojoExecutionException("File " + pluginFile.getName() + " cannot be read");
        }

        if (skipRemoveBefore) {
            logger.info("Skipping removeBefore");
        } else if(this.removeBefore.isEmpty()) {
            logger.debug("removeBefore-list is empty.");
        } else {
            List<String> installedPluginKeysToRemove = upmRestClient.getInstalledPluginKeys().stream()
                    .filter(key -> this.removeBefore.contains(key))
                    .collect(Collectors.toList());

            if(installedPluginKeysToRemove.isEmpty()) {
                logger.info("No installed plugins are to be removed before installation.");
            } else {
                for(String pluginKeyToRemove : installedPluginKeysToRemove) {
                    logger.info("Removing {}", pluginKeyToRemove);
                    upmRestClient.removePlugin(pluginKeyToRemove);
                }
            }
        }

        for(String identifier : installBefore) {
            File installBeforeFile = getPluginFile(identifier);
            logger.info("Installing {} before", installBeforeFile.getAbsolutePath());
            upmRestClient.installPlugin(installBeforeFile);
        }

        logger.info("Installing {}", pluginFile.getAbsolutePath());
        upmRestClient.installPlugin(pluginFile);

        if (pluginLicenseString != null && !pluginLicenseString.trim().isEmpty()) {
            logger.info("Installing license {}", pluginLicenseString);
            installLicense();
        } else {
            logger.debug("No license available for installation");
        }

        for(String identifier : installAfter) {
            File installAfterFile= getPluginFile(identifier);
            upmRestClient.installPlugin(installAfterFile);
        }
    }


    private File getPluginFile(String identifier) {
        if(isMavenCoordinates(identifier)) {
            logger.debug("{} looks like maven coordinates",identifier);
            return resolveFileForMavenArtifact(identifier);
        } else {
            return new File(identifier);
        }
    }

    private boolean isMavenCoordinates(String identifier) {
        return identifier.matches(".*:.*.:.*");
    }

    private File resolveFileForMavenArtifact(String mavenCoordinates) {

        logger.info("Trying to resolve {}", mavenCoordinates);
        Artifact artifact = new DefaultArtifact(mavenCoordinates);

        ArtifactRequest request = new ArtifactRequest();
        request.setRepositories(remoteRepos);
        request.setArtifact(artifact);

        String pathToFile;
        try {
            ArtifactResult result = repoSystem.resolveArtifact(repoSession, request);
            pathToFile = result.getArtifact().getFile().getPath();
        } catch (ArtifactResolutionException e) {
            logger.info("Resolving Artifact failed: {}, returning locally resolved artifact", e.getMessage());
            pathToFile = repoSession.getLocalRepositoryManager().getPathForLocalArtifact(artifact);
        }
        logger.debug("Resolved file {}", pathToFile);
        return new File(pathToFile);
    }
}
