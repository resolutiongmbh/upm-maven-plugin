package de.resolution.maven.upm;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="remove", defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST)
public class RemovePluginMojo extends AbstractUpmMojo {

	public void doExecute() throws MojoExecutionException {
		super.deletePlugin();
	}
}
