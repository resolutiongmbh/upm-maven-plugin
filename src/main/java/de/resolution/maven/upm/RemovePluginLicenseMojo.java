package de.resolution.maven.upm;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="licenseRemove", requiresProject = false)
public class RemovePluginLicenseMojo extends AbstractUpmMojo {

	public void doExecute() throws MojoExecutionException {
		super.removeLicense();
	}
}
