package de.resolution.maven.upm;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="install", requiresProject = false)
public class InstallPluginMojo extends AbstractUpmMojo {

	public void doExecute() throws MojoExecutionException {
		super.installPlugin();
	}
}
