package de.resolution.maven.upm;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="reinstall", requiresProject = false, defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST)
public class ReInstallPluginMojo extends AbstractUpmMojo {

	public void doExecute() throws MojoExecutionException {
		super.reinstallPlugin();
	}
}
