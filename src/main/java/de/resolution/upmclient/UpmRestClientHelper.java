package de.resolution.upmclient;

import okhttp3.HttpUrl;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UpmRestClientHelper {

    public static final String UPM_URL_PATH = "rest/plugins/1.0/";

    @Nonnull
    public HttpUrl createUpmUrl(@Nonnull String baseUrl) {
        HttpUrl baseUrlAsUrl = HttpUrl.parse(baseUrl);
        if(baseUrlAsUrl == null) {
            throw new IllegalArgumentException("Failed to parse " + baseUrl + " to URL");
        }
        return baseUrlAsUrl.newBuilder()
                .addPathSegments(UPM_URL_PATH)
                .build();
    }

    @Nonnull
    public String readBodyAsString(@Nonnull Response response) {
        ResponseBody responseBody = response.body();
        if(responseBody == null) {
            throw new UpmRestClientException.UnexpectedResponseException("Response body must not be null.");
        }
        try {
            return responseBody.string();
        } catch (IOException e) {
            throw new UpmRestClientException.UnexpectedResponseException("Reading the response body failed.",e);
        }
    }

    @Nonnull
    public List<String> parsePluginKeysFromResponse(@Nonnull Response response) {
        return parsePluginKeysFromResponse(readBodyAsString(response));
    }

    @Nonnull
    public List<String> parsePluginKeysFromResponse(@Nonnull String responseString) {

        JSONObject jsonObject = new JSONObject(responseString);
        JSONArray pluginArray = jsonObject.getJSONArray("plugins");

        List<String> keys = new ArrayList<>();

        for(int i=0; i<pluginArray.length(); i++) {
            JSONObject currentPlugin = pluginArray.getJSONObject(i);
            String pluginKey = currentPlugin.getString("key");
            keys.add(pluginKey);
        }
        return keys;
    }

    public boolean listContainsPluginKey(@Nonnull List<String> pluginKeys, @Nonnull String pluginKey) {
        return pluginKeys.contains(pluginKey);
    }
}
