package de.resolution.upmclient;

import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UpmRestClient {

    private static final Logger logger = LoggerFactory.getLogger(UpmRestClient.class);

    private static final String AUTH_HEADER = "Authorization";
    private static final String TOKEN_PARAMETER = "token";
    private static final String KEY_PING_AFTER = "pingAfter";

    public static final int DEFAULT_TIMEOUT = 30;

    private final HttpUrl hostUrl;
    private final HttpUrl upmUrl;
    private final OkHttpClient okHttpClient;
    private final String credentials;
    private final long timeout;

    private final UpmRestClientHelper upmRestClientHelper = new UpmRestClientHelper();

    public UpmRestClient(
            @Nonnull String baseUrlString,
            @Nonnull String username,
            @Nonnull String password,
            long timeout) {
        this.upmUrl  = upmRestClientHelper.createUpmUrl(baseUrlString);
        this.hostUrl = HttpUrl.parse(baseUrlString);
        this.credentials  = Credentials.basic(username, password);

        this.timeout  = timeout;



        this.okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new LoggingInterceptor(logger))
                .cookieJar(new SimpleCookieJar())
                .build();

        logger.debug("UPM-URL is {}", upmUrl);
    }


    public UpmRestClient(
            @Nonnull String baseUrlString,
            @Nonnull String username,
            @Nonnull String password) {
       this(baseUrlString, username, password, DEFAULT_TIMEOUT);
    }


    public List<String> getInstalledPluginKeys() {
        return getTokenAndInstalledPluginKeys().getPluginKeys();
    }


    public TokenAndInstalledPluginKeys getTokenAndInstalledPluginKeys() {
        Request getPluginsRequest = new Request.Builder()
                .get()
                .url(upmUrl)
                .header(AUTH_HEADER, credentials)
                .build();
        try {
            Response response = okHttpClient.newCall(getPluginsRequest).execute();
            if (!response.isSuccessful()) {
                throw new UpmRestClientException.UnexpectedResponseException("HTTP Request failed: " + response.code() + " " + response.message() + " " + response.body().string());
            }
            List<String> pluginKeys = upmRestClientHelper.parsePluginKeysFromResponse(response);
            String token = response.header("upm-token");
            if (token == null) {
                throw new UpmRestClientException.UnexpectedResponseException("No upm-token header in response");
            }

            return new TokenAndInstalledPluginKeys(token, pluginKeys);
        } catch (IOException e) {
            throw new UpmRestClientException.RequestFailedException(e);
        }
    }

    public boolean isPluginInstalled(@Nonnull String pluginKey) {
        return getInstalledPluginKeys().contains(pluginKey);
    }

    public void removePlugin(@Nonnull String pluginKey) {

        TokenAndInstalledPluginKeys tokenAndInstalledPluginKeys = getTokenAndInstalledPluginKeys();
        if (!upmRestClientHelper.listContainsPluginKey(tokenAndInstalledPluginKeys.getPluginKeys(), pluginKey)) {
            throw new UpmRestClientException.NotFoundException();
        }

        HttpUrl deleteUrl = upmUrl.newBuilder()
                .addPathSegments(pluginKey + "-key")
                .addQueryParameter(TOKEN_PARAMETER, tokenAndInstalledPluginKeys.getToken())
                .build();

        Request deletePluginRequest = new Request.Builder()
                .delete()
                .url(deleteUrl)
                .header(AUTH_HEADER, credentials)
                .build();

        try (Response deleteResponse = okHttpClient.newCall(deletePluginRequest).execute()) {
            int code = deleteResponse.code();
            if (code == 404) {
                throw new UpmRestClientException.NotFoundException();
            }
            if (code != 204) {
                throw new UpmRestClientException.UnexpectedResponseException(deleteResponse);
            }
        } catch (IOException e) {
            throw new UpmRestClientException.RequestFailedException(e);
        }
    }

    public void installPlugin(@Nonnull File pluginFile) {

        if(!pluginFile.canRead()) {
            throw new IllegalArgumentException("Cannot read " + pluginFile.getAbsolutePath());
        }

        String token = getTokenAndInstalledPluginKeys().getToken();

        RequestBody postBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                        "plugin",
                        pluginFile.getName(),
                        RequestBody.create(
                                MediaType.parse("application/java-archive"),
                                pluginFile))
                .build();

        HttpUrl postUrl = upmUrl.newBuilder()
                .addQueryParameter(TOKEN_PARAMETER, token)
                .build();

        Request request = new Request.Builder()
                .url(postUrl)
                .post(postBody)
                .header("X-Atlassian-Token", "no-check")
                .header(AUTH_HEADER, credentials)
                .build();

        OkHttpClient postClient = okHttpClient.newBuilder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .build();

        try (Response response = postClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new UpmRestClientException.UnexpectedResponseException(response);
            }

            logger.info("Installation started");

            String responseString = getBodyString(response);
            logger.debug("Response: {}",responseString);

            JSONObject jsonObject = new JSONObject(responseString);

            JSONObject statusObj = jsonObject.getJSONObject("status");
            if (statusObj == null) {
                throw new UpmRestClientException.ReadJsonFailedException("No status in response");
            }
            int statusCode = statusObj.getInt("statusCode");
            if(statusCode != 200) {
                throw new UpmRestClientException.UnexpectedResponseException("Unexpected statusCode in response: " + statusCode);
            }
            boolean done = statusObj.getBoolean("done");

            if (!done) {
                int pingAfter = jsonObject.getInt(KEY_PING_AFTER);
                JSONObject links = jsonObject.getJSONObject("links");
                String statusUrl = links.getString("self");
                pingStatus(hostUrl.newBuilder().encodedPath(statusUrl).build(), pingAfter);
            } else {
                logger.debug("Installation is done, no need to ping status.");
            }
        } catch (IOException e) {
            throw new UpmRestClientException.RequestFailedException(e);
        }
    }

    private void pingStatus(HttpUrl statusUrl, int waitBeforeStartInMilliSeconds) throws IOException {

        sleep(waitBeforeStartInMilliSeconds);

        Request pingRequest = new Request.Builder()
                .get()
                .url(statusUrl)
                .header(AUTH_HEADER, credentials)
                .build();

        int lastProgress = 0;
        boolean done = false;
        while (!done) {
            String pingResponseString;
            JSONObject pingObject;

            try (Response pingResponse = okHttpClient.newCall(pingRequest).execute()) {
                if (!pingResponse.isSuccessful()) {
                    throw new UpmRestClientException.UnexpectedResponseException(pingResponse);
                }
                pingResponseString = getBodyString(pingResponse);
                pingObject = new JSONObject(pingResponseString);
            }

            if (pingObject.has("enabled")) {
                if (pingObject.getBoolean("enabled")) {
                    logger.info("Installation is Done");
                    return;
                }
                throw new UpmRestClientException.InstallationFailedException(pingResponseString);
            }

            if (!pingObject.has(KEY_PING_AFTER)) {
                throw new UpmRestClientException.UnexpectedResponseException("Unexpected response data: " + pingResponseString);
            }

            int pingAfter = pingObject.getInt(KEY_PING_AFTER);
            JSONObject statusObj = pingObject.getJSONObject("status");

            if (statusObj == null) {
                throw new UpmRestClientException.UnexpectedResponseException("No Status in Response");
            }
            done = statusObj.getBoolean("done");

            if (statusObj.has("subCode")) {
                String subCode = statusObj.getString("subCode");
                throw new UpmRestClientException.InstallationFailedException(subCode);
            }

            if(statusObj.has("amountDownloaded")) {
                int progress = statusObj.getInt("amountDownloaded");
                if (progress != lastProgress) {
                    lastProgress = progress;
                    logger.info("Installation is in progress: {}%", progress);
                }
            } else {
                logger.info("No progress information available in {}", statusObj);
            }

            sleep(pingAfter);
        }
    }

    public void installLicense(@Nonnull String pluginKey, @Nonnull String pluginLicenseString) {

        if(pluginKey.trim().isEmpty()) {
            throw new IllegalArgumentException("Plugin key must not be empty");
        }

        if(pluginLicenseString.trim().isEmpty()) {
            throw new IllegalArgumentException("License String must not be empty");
        }

        logger.info("Installing license for {}: {}",pluginKey,pluginLicenseString);

        HttpUrl licenseUrl = upmUrl.newBuilder()
                .addPathSegment(pluginKey + "-key")
                .addPathSegment("license")
                .build();
        String jsonString = "{\"rawLicense\" : \"" + pluginLicenseString + "\"}";
        RequestBody body = RequestBody.create(MediaType.parse("application/vnd.atl.plugins+json"), jsonString);

        Request installLicenseRequest = new Request.Builder()
                .put(body)
                .url(licenseUrl)
                .header(AUTH_HEADER, credentials)
                .header("nosso", "true")
                .build();

        try (Response response = okHttpClient.newCall(installLicenseRequest).execute()) {

            if(response.body() == null) {
                throw new UpmRestClientException.NoResponseBodyException();
            }
            String responseBody = response.body().string();

            if (isFail(response.code())) {
                JSONObject statusObject = new JSONObject(responseBody);
                String subCode = statusObject.getString("subCode");

                if("upm.plugin.license.error.invalid.update".equals(subCode)) {
                    logger.warn("Installing license failed with SubCode upm.plugin.license.error.invalid.update, this is expected if the same license is already installed");
                    return;
                } else {
                    throw new UpmRestClientException.LicenseFailedException(responseBody);
                }
            }

            logger.info("Installed license");
            logger.debug(responseBody);

            JSONObject resultObject = new JSONObject(responseBody);
            boolean valid = resultObject.getBoolean("valid");

            if(!valid) {
                throw new UpmRestClientException.LicenseFailedException("License is not valid: " + responseBody);
            }
        } catch (IOException e) {
            throw new UpmRestClientException.RequestFailedException(e);
        }
    }

    public void removeLicense(@Nonnull String pluginKey) {

        logger.info("Removing License for {}", pluginKey);

        HttpUrl licenseUrl = upmUrl.newBuilder()
                .addPathSegment(pluginKey + "-key")
                .addPathSegment("license")
                .build();

        Request removeLicenseRequest = new Request.Builder()
                .delete()
                .url(licenseUrl)
                .header(AUTH_HEADER, credentials)
                .build();

        try (Response response = okHttpClient.newCall(removeLicenseRequest).execute()) {

            String responseBody = (response.body() != null) ? response.body().string() : "no body in response";

            if (isFail(response.code())) {
                throw new UpmRestClientException.UnexpectedResponseException("Removing license failed: " + responseBody);
            }
            logger.debug(responseBody);

        } catch (IOException e) {
            throw new UpmRestClientException.RequestFailedException(e);
        }
    }

    public LicenseData getLicense(@Nonnull String pluginKey) {

        if(pluginKey.trim().isEmpty()) {
            throw new IllegalArgumentException("Plugin key must not be empty");
        }

        HttpUrl licenseUrl = upmUrl.newBuilder().addPathSegments(pluginKey + "-key/license").build();

        Request getLicenseRequest = new Request.Builder()
                .get()
                .url(licenseUrl)
                .header(AUTH_HEADER, credentials)
                .build();

        try (Response response = okHttpClient.newCall(getLicenseRequest).execute()) {

            if(response.body() == null) {
                throw new UpmRestClientException.NoResponseBodyException();
            }
            String responseBody = response.body().string();

            logger.info("==== Result: {}", responseBody);

            return LicenseData.fromJson(responseBody);

        } catch (IOException e) {
            throw new UpmRestClientException.RequestFailedException(e);
        }
    }

    private String getBodyString(@Nonnull Response response) {
        ResponseBody body = response.body();
        if(body == null) {
            throw new UpmRestClientException.NoResponseBodyException();
        }
        try {
            return body.string();
        } catch (IOException e) {
            throw new UpmRestClientException.ReadingBodyFailedException(e);
        }
    }

    private void sleep(long msecs) {
        try {
            Thread.sleep(msecs);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new UpmRestClientException.AbortedException();
        }
    }

    private boolean isFail(int httpCode) {
        return (httpCode < 200 || httpCode >= 300);
    }

    public static class TokenAndInstalledPluginKeys {

        private final List<String> pluginKeys;
        private final String token;

        public TokenAndInstalledPluginKeys(@Nonnull String token, @Nonnull List<String> pluginKeys) {
            this.token = token;
            this.pluginKeys = new ArrayList<>(pluginKeys);
        }

        @Nonnull
        public List<String> getPluginKeys() {
            return Collections.unmodifiableList(pluginKeys);
        }

        @Nonnull
        public String getToken() {
            return token;
        }
    }
}
