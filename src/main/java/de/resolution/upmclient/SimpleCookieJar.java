package de.resolution.upmclient;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SimpleCookieJar implements CookieJar {

    private final Map<String,Cookie> storage = new HashMap<>();

    @Nonnull
    @Override
    public List<Cookie> loadForRequest(@Nonnull HttpUrl httpUrl) {
        // Remove expired Cookies
        storage.values().removeIf(cookie -> cookie.expiresAt() < System.currentTimeMillis());

        // Only return matching Cookies
        return storage.values().stream().filter(cookie -> cookie.matches(httpUrl)).collect(Collectors.toList());
    }

    @Override
    public void saveFromResponse(@Nonnull HttpUrl httpUrl, @Nonnull List<Cookie> cookies) {

        cookies.forEach(cookie -> {
            storage.put(cookie.name(),cookie);
        });
    }
}
