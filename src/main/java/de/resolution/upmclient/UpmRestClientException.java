package de.resolution.upmclient;

import okhttp3.Response;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;

public abstract class UpmRestClientException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(UpmRestClientException.class);

    protected UpmRestClientException(String message, Throwable cause) {
        super(message,cause);
    }

    protected UpmRestClientException(String message) {
        super(message);
    }

    public static class NotFoundException extends UpmRestClientException {
        public NotFoundException() {
            super("Not found");
        }
    }

    public static class UnexpectedResponseException extends UpmRestClientException {
        public UnexpectedResponseException(String message) {
            super(message,null);
        }

        public UnexpectedResponseException(String message, Throwable cause) {
            super(message, cause);
        }

        public UnexpectedResponseException(@Nonnull Response response) {
            super("Unexpected Response: " + response.code() + " " + response.message() + response.toString());
            try {
                ResponseBody body = response.body();
                if(body != null) {
                    logger.warn("Body: {} ", body.string());
                } else {
                    logger.warn("Response contains no body");
                }
            } catch (IOException e) {
                logger.warn("Reading body failed",e);
            }

        }
    }

    public static class RequestFailedException extends UpmRestClientException {
        public RequestFailedException(Throwable cause) {
            super("HTTP Request failed",cause);
        }
    }

    public static class NoResponseBodyException extends UpmRestClientException {
        public NoResponseBodyException() {
            super("Response body os null!");
        }
    }

    public static class ReadingBodyFailedException extends UpmRestClientException {
        public ReadingBodyFailedException(Throwable cause) {
            super("Reading body failed",cause);
        }
    }

    public static class ReadJsonFailedException extends UpmRestClientException {
        public ReadJsonFailedException(String message) {
            super(message);
        }
    }

    public static class AbortedException extends UpmRestClientException {
        public AbortedException() {
            super("Aborted");
        }
    }

    public static class InstallationFailedException extends UpmRestClientException {
        public InstallationFailedException(String message) {
            super("Installation failed: " + message);
        }
    }

    public static class LicenseFailedException extends UpmRestClientException {
        public LicenseFailedException(String message) {
            super("Installing license failed : " + message);
        }
    }
}
