package de.resolution.upmclient;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import java.io.IOException;

public class LoggingInterceptor implements Interceptor {

    private final Logger logger;

    public LoggingInterceptor(@Nonnull Logger logger) {
        this.logger = logger;
    }

    @Override
    @Nonnull
    public Response intercept(@Nonnull Interceptor.Chain chain) throws IOException {

        if (!logger.isDebugEnabled()) {
            return chain.proceed(chain.request());
        }

        Request request = chain.request();

        logger.debug("--> Request: {} {}", request.method(), request.url());
        request.headers().forEach(
                pair -> logger.debug("--> Header: {} = {}", pair.getFirst(), pair.getSecond()));

        Response response = chain.proceed(request);

        logger.debug("<-- Response: {} {}", response.code(), response.message());
        response.headers().forEach(
                pair -> logger.debug("<-- Header: {} = {}", pair.getFirst(), pair.getSecond()));

        return response;
    }
}

