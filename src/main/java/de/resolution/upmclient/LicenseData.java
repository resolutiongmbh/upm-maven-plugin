package de.resolution.upmclient;

import org.json.JSONObject;

import javax.annotation.Nonnull;

public class LicenseData {

    private final boolean valid;
    private final boolean active;
    private final String pluginKey;
    private final boolean evaluation;
    private final boolean dataCenter;
    private final boolean maintenanceExpired;
    private final String rawLicense;
    private final long maintenanceExpiryDate;

    public static LicenseData fromJson(@Nonnull String jsonString) {

        JSONObject jsonObject = new JSONObject(jsonString);

        boolean valid      = jsonObject.has("valid") && jsonObject.getBoolean("valid");
        boolean active     = jsonObject.has("active") && jsonObject.getBoolean("active");
        String pluginKey   = jsonObject.has("pluginKey") ? jsonObject.getString("pluginKey") : null;
        boolean evaluation = jsonObject.has("evaluation") && jsonObject.getBoolean("evaluation");
        boolean datacenter = jsonObject.has("dataCenter") && jsonObject.getBoolean("dataCenter");
        String rawLicense =  jsonObject.has("rawLicense") ? jsonObject.getString("rawLicense") : null;
        boolean maintenanceExpired = jsonObject.has("maintenanceExpired") && jsonObject.getBoolean("maintenanceExpired");
        long maintenanceExpiryDate = jsonObject.has("maintenanceExpiryDate") ? jsonObject.getLong("maintenanceExpiryDate") : 0L;

        return new LicenseData(valid,active,pluginKey,evaluation,datacenter,rawLicense,maintenanceExpired,maintenanceExpiryDate);
    }

    public LicenseData(
            boolean valid,
            boolean active,
            String pluginKey,
            boolean evaluation,
            boolean datacenter,
            String rawLicense,
            boolean maintenanceExpired,
            long maintenanceExpiryDate) {
        this.valid = valid;
        this.active = active;
        this.pluginKey = pluginKey;
        this.evaluation = evaluation;
        this.dataCenter = datacenter;
        this.rawLicense = rawLicense;
        this.maintenanceExpired = maintenanceExpired;
        this.maintenanceExpiryDate = maintenanceExpiryDate;
    }

    public boolean isValid() {
        return valid;
    }

    public boolean isActive() {
        return active;
    }

    public String getPluginKey() {
        return pluginKey;
    }

    public boolean isEvaluation() {
        return evaluation;
    }

    public boolean isDataCenter() {
        return dataCenter;
    }

    public boolean isMaintenanceExpired() {
        return maintenanceExpired;
    }

    public String getRawLicense() {
        return rawLicense;
    }

    public long getMaintenanceExpiryDate() {
        return maintenanceExpiryDate;
    }
}
