package ut.de.resolution.upmclient;

import de.resolution.upmclient.LicenseData;
import it.de.resolution.upmclient.TestUtil;
import org.junit.Test;

import static org.junit.Assert.*;

public class LicenseDataTest {

    @Test
    public void loadFromValidResponse() {

        String json = TestUtil.readTextFileFromClasspath("/valid_license_response.json");

        LicenseData licenseData = LicenseData.fromJson(json);
        assertTrue(licenseData.isValid());
        assertTrue(licenseData.isActive());
        assertEquals("de.resolution.httpheaderauth.jira", licenseData.getPluginKey());
        assertFalse(licenseData.isEvaluation());
        assertFalse(licenseData.isDataCenter());
        assertEquals(
                "AAABuw0ODAoPeNqNUu1u2jAU/e+nsLQ/mypCPloISJYGIYWhAhWkU4X44yQX4i5xomuHjT3PnqEv0BerG8jW/dokW7J8dM859577IaqB3kJMbZ86ztC7Ht70abCJqGu7LvkGp6+ASpSSOT3b7tu+5zlkWRcx4Gr/oAzGOg4JSql5ope8ADZ/ecYDHSOXqdIvv7Qm9zUmGVcw4RrYG23HNscndyIBqSA6VdBUBqvFIlwHX0Z3LRT+qASemrpa5qIQGtJWLFxwkbMnKz4rcdD6M4Iq81obu1YKJAXr3UemdZUBTwF5rTPrSSC3NpqjBmR7nisg+Vl0xlXGFsH34HYS1tNusfYeT4Ne9/jYs5N0G17Jn3k/HmXr5XY6zmt5VQXl9fZGzJN9JGHeHY3xsGM79h/ypgtxBKaxBmK6kRokl8n7rs20vMu0/k0HhgArFOpCuQmXzNyOO/C9nusPyAoPXArF3xjY+jcZnRbxjAQIDfB3SoN2Ku0auGQCKkFRNSSzKLqns8YHHRkjxoJIGhq6L5HOja0/j6BEoB83gEfAT/ShDZQ2a7QbGrwoABPBc3rJn4RHntdnv+eMXgGVHO9qMCwCFA+8DeAil/pcSTHf5WdiGCYxIDmMAhRnqow6YzXAnMbzghTOU/AWime6ag==X02l9",
                licenseData.getRawLicense());
        assertFalse(licenseData.isMaintenanceExpired());
        assertEquals(1675774800000L, licenseData.getMaintenanceExpiryDate());
    }

    @Test
    public void loadFromNotExistingResponse() {

        String json = TestUtil.readTextFileFromClasspath("/not_existing_license_response.json");

        LicenseData licenseData = LicenseData.fromJson(json);
        assertFalse(licenseData.isValid());
        assertFalse(licenseData.isActive());
        assertEquals("some.non.existing.key", licenseData.getPluginKey());
        assertFalse(licenseData.isEvaluation());
        assertFalse(licenseData.isDataCenter());
        assertNull(licenseData.getRawLicense());
        assertFalse(licenseData.isMaintenanceExpired());
        assertEquals(0L, licenseData.getMaintenanceExpiryDate());
    }
}
