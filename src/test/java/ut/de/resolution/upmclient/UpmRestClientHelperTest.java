package ut.de.resolution.upmclient;

import de.resolution.upmclient.UpmRestClientHelper;
import it.de.resolution.upmclient.TestUtil;
import okhttp3.HttpUrl;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class UpmRestClientHelperTest {

    private UpmRestClientHelper upmRestClientHelper;

    @Before
    public void setup() {
        this.upmRestClientHelper = new UpmRestClientHelper();
    }

    @Test
    public void createUpmUrl_withProperUrl_shouldContainUpmPath() {
        HttpUrl upmUrl = upmRestClientHelper.createUpmUrl("https://jira.example.com/");
        Assert.assertTrue(upmUrl.toString().endsWith(UpmRestClientHelper.UPM_URL_PATH));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUpmUrl_withInvalidUrl_shouldFail() {
        upmRestClientHelper.createUpmUrl("invalidUrl");
    }

    @Test
    public void parsePluginKeysFromResponse_withEmptyArray_returnsEmptyList() {
        String json = "{\"plugins\": [ ] }";
        List<String> keys = upmRestClientHelper.parsePluginKeysFromResponse(json);
        Assert.assertTrue(keys.isEmpty());
    }

    @Test(expected = JSONException.class)
    public void parsePluginKeysFromResponse_withEmptyString_throwsJSONException() {
        upmRestClientHelper.parsePluginKeysFromResponse("");
    }

    @Test(expected = JSONException.class)
    public void parsePluginKeysFromResponse_withInvalidJson_throwsJSONException() {
        upmRestClientHelper.parsePluginKeysFromResponse("asf {}");
    }

    @Test(expected = JSONException.class)
    public void parsePluginKeysFromResponse_withMissingPluginsKey_throwsJSONException() {
        String json = "{\"plug\": [ ] }";
        upmRestClientHelper.parsePluginKeysFromResponse(json);
    }

    @Test(expected = JSONException.class)
    public void parsePluginKeysFromResponse_withMissingPluginKeyInArray_throwsJSONException() {
        String json = "{\"plugins\": [ \"abc\" : { \"noKey\": {} ] }";
        upmRestClientHelper.parsePluginKeysFromResponse(json);
    }

    @Test
    public void parsePluginKeysFromResponse_withValidResponse() {
        String responseString = TestUtil.readTextFileFromClasspath("/pluginListResponse.json");
        List<String> keys = upmRestClientHelper.parsePluginKeysFromResponse(responseString);
        Assert.assertFalse(keys.isEmpty());
        Assert.assertTrue(keys.contains("de.resolution.usersync.confluence"));
        Assert.assertFalse(keys.contains("de.resolution.usersync.jira"));
    }

    @Test
    public void listContainsPluginKey_withContainingElement() {
        String pluginKey = "de.resolution.de.resolution.usersync.confluence";
        List<String> keyList = Arrays.asList("abc.de","bla","blub",pluginKey);
        Assert.assertTrue(upmRestClientHelper.listContainsPluginKey(keyList,pluginKey));
    }

    @Test
    public void listContainsPluginKey_withoutContainingElement() {
        String pluginKey = "de.resolution.de.resolution.usersync.confluence";
        List<String> keyList = Arrays.asList("abc.de","bla","blub");
        Assert.assertFalse(upmRestClientHelper.listContainsPluginKey(keyList,pluginKey));
    }

}
