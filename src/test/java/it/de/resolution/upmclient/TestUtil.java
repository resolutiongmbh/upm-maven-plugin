package it.de.resolution.upmclient;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

public class TestUtil {

    public static void setPrivateField(Object object, String fieldName, String fieldValue) throws NoSuchFieldException, IllegalAccessException {
        setPrivateField(object,fieldName,fieldValue,object.getClass());
    }

    private static void setPrivateField(Object object, String fieldName, String fieldValue, Class<?> clazz) throws NoSuchFieldException, IllegalAccessException {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            String fieldTypeName = field.getType().getName();
            if(fieldTypeName.equals("long")) {
                field.set(object,Long.parseLong(fieldValue));
            } else {
                field.set(object, fieldValue);
            }

        } catch(NoSuchFieldException e) {
            if(clazz.getSuperclass() != null) {
                setPrivateField(object,fieldName,fieldValue,clazz.getSuperclass());
            } else {
                throw e;
            }
        }
    }

    public static String readTextFileFromClasspath(String filename) {
        InputStream in = TestUtil.class.getResourceAsStream(filename);
        if (in == null) {
            throw new IllegalArgumentException("Nothing found under " + filename);
        } else {
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");
            String ret = scanner.hasNext() ? scanner.next() : "";
            scanner.close();
            return ret;
        }
    }

    public static File getFileFromClasspath(String filename) {
        URL urlToFile = TestUtil.class.getResource(filename);
        if(urlToFile == null) {
            throw new IllegalArgumentException("Nothing found under " + filename);
        }
        try {
            return new File(urlToFile.toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("cannot create URI from " + urlToFile);
        }
    }
}
