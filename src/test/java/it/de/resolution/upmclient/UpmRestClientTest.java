package it.de.resolution.upmclient;

import de.resolution.upmclient.LicenseData;
import de.resolution.upmclient.UpmRestClient;
import de.resolution.upmclient.UpmRestClientException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class UpmRestClientTest {

    private static final String USERNAME;
    private static final String PASSWORD;
    private static final String UPM_URL_STRING;

    static {
        try {
            Properties prop = new Properties();
            prop.load(UpmRestClient.class.getResourceAsStream("/testdata.properties"));
            USERNAME = prop.getProperty("upm.user");
            PASSWORD = prop.getProperty("upm.password");
            UPM_URL_STRING = prop.getProperty("upm.host");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static final String TEST_PLUGIN_KEY = "de.resolution.atlas-http-dumper";
    private static final String NOT_INSTALLED_KEY = "not.installed.key";

    private final File pluginFile = TestUtil.getFileFromClasspath("/atlas-http-dumper-1.0.0.jar");

    private static final String HEADER_AUTH_PLUGIN_KEY = "de.resolution.httpheaderauth.jira";
    private static final String HEADER_AUTH_PLUGIN_LICENSE = "AAAB0w0ODAoPeNqNkt1um0AQhe/3KVbqTXthBMZ/sbRSHUxru8aODLaiyDcLHsI6sKBhSeI8T5+hL5AX6wJ2k0qVGmm5mT1nmO/sfPJySacQUcui5mhsDsa9K+r4Ae2aXZs4CFyJXE65AlZXOlZXH7IUEcgSglMBK54Bc9ae526c+WRJDmAglHla1T4jUapIgB8AeaUS4yiQG7oZd0AqQKawAjKFMkJR1Ho2C4IbOmsMdKIdWiaiZgQa50gX2k9rP20bvBX9PFZPHIF+fnf9hW5lKjKh4EC3JWC5H1MnzzLASPCUBoAZPaMQjwttkVxG4D4XAk8X5l7DbJFVlYWA67hpxDrWJYR/q9P2cqe1NVeX+FX4htlwu488rRo2FvNUj7DGey5F2ZY2f0Kk37NwRpxcKh4pV8+ZsqMRIpeHUnFQ6uu7vA9w+fOMlwnznCfnm7MIq2BbHHfqx7S3XSa9611s+30bi5dJsvH9u37xcDcI5t1ocHsbmzLZ5PPnvisne7Zn5AFOFwhrYJpDc2TbFrmpMEp4CX8vhvWB19cM4hHaBP6v9hXHelHagM4ZNCu3eP2F9/S6jeH1p1LEd1dMf52+NbKvhqPhB9o3W1KgKM8D/QaOYREcMC0CFQCOrQ7LeKWmzZsgzBw0rTzQyJzIZQIUTtxhtKTjFapGWNU/iJcb1kd75Hk=X02ma";
    private static final File headerAuthPluginFile = TestUtil.getFileFromClasspath("/de.resolution.httpheaderauth.jira-1.2.0.jar");

    private UpmRestClient upmRestClient;

    private static final Logger logger = LoggerFactory.getLogger(UpmRestClientTest.class);

    @Before
    public void setup() {
        this.upmRestClient = new UpmRestClient(UPM_URL_STRING, USERNAME, PASSWORD);
    }

    @Test
    public void getTokenAndInstalledPluginKeys_notEmpty() {
        UpmRestClient.TokenAndInstalledPluginKeys tokenAndInstalledPluginKeys = upmRestClient.getTokenAndInstalledPluginKeys();
        Assert.assertNotNull(tokenAndInstalledPluginKeys.getToken());
        Assert.assertFalse(tokenAndInstalledPluginKeys.getPluginKeys().isEmpty());
    }

    @Test
    public void getInstalledPluginKeys_isNotEmpty() {
        List<String> keys = upmRestClient.getInstalledPluginKeys();
        Assert.assertFalse(keys.isEmpty());
    }

    @Test
    public void isPluginInstalledForNonInstalledPlugin_returnsFalse() {
        Assert.assertFalse(upmRestClient.isPluginInstalled(NOT_INSTALLED_KEY));
    }

    @Test(expected = UpmRestClientException.NotFoundException.class)
    public void removeNonInstalledPlugin_throwNotFoundException() {
        upmRestClient.removePlugin(NOT_INSTALLED_KEY);
    }

    @Test
    public void removeInstalledPlugin_pluginIsRemoved() {
        if (!upmRestClient.isPluginInstalled(TEST_PLUGIN_KEY)) {
            logger.info("{} is not installed yet, installing now.", TEST_PLUGIN_KEY);
            upmRestClient.installPlugin(pluginFile);
        }
        upmRestClient.removePlugin(TEST_PLUGIN_KEY);
        Assert.assertFalse("Plugin is still there", upmRestClient.getInstalledPluginKeys().contains(TEST_PLUGIN_KEY));
    }

    @Test(expected = IllegalArgumentException.class)
    public void installPlugin_withInvalidFile_throwsException() {
        upmRestClient.installPlugin(new File("/some/non/existing/path"));
    }

    @Test
    public void getNonInstalledLicense_returnsLicense() {
        LicenseData licenseData = upmRestClient.getLicense("some.non.existing.key");
        Assert.assertFalse(licenseData.isActive());
        Assert.assertFalse(licenseData.isValid());
    }

    @Test
    public void installPlugin_pluginIsInstalled() {

        Assert.assertTrue(pluginFile.canRead());

        if (upmRestClient.isPluginInstalled(TEST_PLUGIN_KEY)) {
            logger.warn("{} is already installed, removing it.", TEST_PLUGIN_KEY);
            upmRestClient.removePlugin(TEST_PLUGIN_KEY);
        }
        upmRestClient.installPlugin(pluginFile);
        Assert.assertTrue(upmRestClient.isPluginInstalled(TEST_PLUGIN_KEY));

        logger.info("Removing plugin again");
        upmRestClient.removePlugin(TEST_PLUGIN_KEY);
    }

    @Test
    public void installPluginAndLicense_pluginIsInstalled() {

        Assert.assertTrue(headerAuthPluginFile.canRead());

        if (upmRestClient.isPluginInstalled(HEADER_AUTH_PLUGIN_KEY)) {
            logger.info("Plugin is installed already");
        } else {
            upmRestClient.installPlugin(headerAuthPluginFile);
            Assert.assertTrue(upmRestClient.isPluginInstalled(HEADER_AUTH_PLUGIN_KEY));
        }

        upmRestClient.removeLicense(HEADER_AUTH_PLUGIN_KEY);
        upmRestClient.installLicense(HEADER_AUTH_PLUGIN_KEY, HEADER_AUTH_PLUGIN_LICENSE);
        LicenseData licenseData = upmRestClient.getLicense(HEADER_AUTH_PLUGIN_KEY);
        Assert.assertTrue(licenseData.isValid());
    }

}
